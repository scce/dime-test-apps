# DIME Test Apps

Monorepository for all DIME test applications

## Generating .gitlab-ci.yml

`.gitlab-ci.yml` is generated from `ci/gitlab-ci.scm`.

### With Nix

- Just run `./generate-gitlab-ci`.

#### Setup pre-commit hook

The pre-commit hook will run `./generate-gitlab-ci` prior to a commit command.
It makes sure the `.gitlab-ci.yml` is in sync with the `ci/gitlab-ci.scm`.

- Run `ci/setup-pre-commit-hook.sh` to setup the hook.

### Without Nix

- Install required dependencies: `guile` (2.2.6), `guile-json` (4.3.2)
- Run `ci/gitlab-ci.scm`

## Linting .gitlab-ci.yml

`.gitlab-ci.yml` will be linted by the public API of `gitlab.com`.

### With Nix

- Just run `./lint-gitlab-ci`.

### Without Nix

- Install required dependencies: `python` (3.6)
- Run `ci/lint.py`
