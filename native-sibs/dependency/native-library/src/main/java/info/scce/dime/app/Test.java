package info.scce.dime.app;

import java.util.HashMap;
import java.util.Map;


public class Test {
	
	public static boolean testBooleanBranch(boolean value) {
		return value;
	}
	
	public static String testFailureBranch(String message) {
		throw new RuntimeException(message);
	}
	
	public static void testSuccessBranchVoid() {
		// do nothing
	}
	
	public static String testSuccessBranchResult(String name) {
		return "Hello " + name;
	}
	
	public static Map<String,String> testMultiBranches(String name, String greeting) {
		Map<String,String> result = new HashMap<>();
		result.put(greeting, "Hi " + name);
		result.put(greeting, "Hello " + name);
		result.put(greeting, "Hey " + name);
		return result;
	}
	
	public static Map<String,String> testMultiOutputs(String name) {
		Map<String,String> result = new HashMap<>();
		result.put("greeting.hi", "Hi " + name);
		result.put("greeting.hello", "Hello " + name);
		result.put("greeting.hey", "Hey " + name);
		return result;
	}

	public static Map<String,String> testMultiBranchMultiOutputs(String name1, String name2) {
		Map<String,String> result = new HashMap<>();
		result.put("hi.name1", "Hi " + name1);
		result.put("hi.name2", "Hi " + name2);
		result.put("hello.name1", "Hello " + name1);
		result.put("hello.name2", "Hello " + name2);
		result.put("hey.name1", "Hey " + name1);
		result.put("hey.name2", "Hey " + name2);
		return result;
	}
}