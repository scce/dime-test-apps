The following changes might be necessary to upgrade an existing Dart plugin to meet the requirements of the new Dart version 2.x

* change the imported angular package from "angular2" to "angular": `import 'package:angular/angular.dart';`
* state the use of the "coreDirectives": `directives: [coreDirectives]`
* as of Angular 4, platform_directives is gone. NgModel is a form directive now. Include them as follows:  
  `import 'package:angular_forms/angular_forms.dart' as forms;`  
  and add it to the specified directives: `directives: [coreDirectives, forms.formDirectives]`
* outputs that utilize `StreamController<dynamic>` respectively `Stream<dynamic>` have to get a little more precise generic types. Change them from `<dynamic>` to `<Map<String,dynamic>>`. 
    Resulting in `StreamController<Map<String,dynamic>>` respectively `Stream<Map<String,dynamic>>`.
* Some local imports within plugins like `import 'package:app/models/Selectives.dart'` have to be adjusted using the additional path `src`, e.g.: `import 'package:app/src/models/Selectives.dart'`.
    Additionally ensure that `Data.dart` uses its new path `data` instead of `models`.
* `style`-tags within html files have to be moved to a separate css file
* Plugins that make use of `BaseSelective` have to either use `BaseModel` or the blank object they tried to wrap instead.

Beside of that there are some other errors within DIMEv2 that need to be fixed. An issue discussing these errors has already been created.

Test11 (workload_state) additionally required to modify the return type of its function `getPercentage()`. Using the `floor()` function does the job.


The following dart plugins used in the Equinocs project have to get tested:
* accept_checkbox --> Test04
* affiliation_listing --> Test01
* ~~all_over_score~~ (does not use dart)
* ~~assignment_progress_click~~ (does not use dart)
* ~~assignment_sort~~ (does not use dart)
* assignment_state --> Test13
* ~~assignment_status~~ (no references found within the app)
* ~~bidding~~ (does not use dart)
* ~~bidding_buttons~~ (no references found within the app)
* ~~close_modal~~ (no references found within the app)
* conference_structure --> Test14
* decide_dropdown --> Test16
* ~~decide_sort~~ (does not use dart)
* default_image --> Test07
* ~~discuss_sort~~ (does not use dart)
* ~~exhaustometer~~ (no references found within the app)
* external_page_frame --> Test05
* ~~favicon~~ (is no plugin at all)
* ~~file_upload~~ (is no plugin at all)
* freeze_assignment_options --> Test20
* ~~hide_tabs~~ (does not use dart)
* hierarchical_property_field --> Test08
* keyword_list --> Test12
* landing_page --> Test21
* notifier --> Test10
* ~~overview~~ (is no plugin at all)
* page_frame (components within the folder that are not listed as one of the next items are not used within the app)
* page_frame/InternalPageFrame --> Test06
* page_frame/MessageNotification --> Test30
* page_frame/Notification --> Test31
* page_frame/SubmitableEntry --> Test32
* paper_status --> Test17
* ~~produce_sort~~ (does not use dart)
* produce_state --> Test22
* ~~report_button~~ (no references found within the app)
* ~~report_progress_click~~ (does not use dart)
* ~~report_score~~ (does not use dart)
* report_stars --> Test18
* ~~reportometer~~ (no references found within the app)
* review_result --> Test23
* review_state --> Test19
* ~~scroll_to_element~~ (probably not properly in use, just the plain js file has real use)
* ~~scroll_to_entry~~ (does not use dart)
* ~~scroll_to_tpaper~~ (no references found within the app)
* stacked_progress_bar --> Test24
* ~~stacked_progress_bar_five~~ (does not use dart)
* subreview_state --> Test25
* subreview_status_bar --> Test26
* ~~text_truncation~~ (no references found within the app)
* ~~time_line~~ (does not use dart)
* ~~timeline_filter~~ (no references found within the app)
* ~~to_top~~ (does not use dart)
* toggle_button --> Test15
* toggle_decision --> Test02 (toggle_decision_basic) / Test03
* toggle_switch --> Test09
* toggle_switch/ToggleSwitchAuthor --> Test34
* toggle_switch/ToggleSwitchPaper --> Test33
* toggle_switch/ToggleSwitchUser --> Test09
* tooltip --> Test28
* user_description --> Test27
* vpat --> Test29
* ~~word_counter~~ (does not use dart)
* workload_state --> Test11
