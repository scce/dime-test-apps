package info.scce.dime.app.testapps;

import java.util.List;


public class Lists {
	/**
	 * Removes the object at the specified index, if existent.
	 * Does not fail if the index is out of bounds.
	 * 
	 * @param index
	 * @param list
	 * @return
	 */
	public static <T> T removeByIndex(long index, T list) {
		List l = ((List) list);
		int i = (int) index;
		if (i >= 0 && i < l.size()) {
			l.remove(i);
		}
		return list;
	}
	
	public static <T> T addAsLast(Object object, T list) {
		List l = (List) list;
		l.add(object);
		return (T) l;
	}
	
}