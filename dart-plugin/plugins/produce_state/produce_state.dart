import 'package:angular/angular.dart';

@Component(
  selector: "ProduceState",
  templateUrl: 'produce_state.html',
  styleUrls: const ["produce_state.css"],
  directives: [coreDirectives]
)

class ProduceState {

  	@Input() bool paper;
  	@Input() bool archive;
  	@Input() bool consent;
  	@Input() bool registrant;
  	
	String getPaperClass() {
		return getClassFor(paper);
	}
	
	String getArchiveClass() {
		return getClassFor(archive);
	}
	
	String getConsentClass() {
		return getClassFor(consent);
	}
	
	String getRegistrantClass() {
		return getClassFor(registrant);
	}
	
	String getClassFor(bool flag) {
	 	if (flag)
			return "";
		return "item-disabled";
	}
	
	String getPaperTooltip() {
		String tooltip = "Final Paper";
		if (!paper)
			return tooltip + " (missing)";
		return tooltip;
	}
	
	String getArchiveTooltip() {
		String tooltip = "Source Archive";
		if (!archive)
			return tooltip + " (missing)";
		return tooltip;
	}
	
	String getConsentTooltip() {
		String tooltip = "Consent to Publish";
		if (!consent)
			return tooltip + " (missing)";
		return tooltip;
	}
	
	String getRegistrantTooltip() {
		String tooltip = "Associated Registrant";
		if (!registrant)
			return tooltip + " (missing)";
		return tooltip;
	}
}
