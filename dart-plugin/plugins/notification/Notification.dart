import 'package:angular/angular.dart';
import 'dart:async';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
  selector: "Notification",
  templateUrl: 'notification.html',
  encapsulation: ViewEncapsulation.None,
  directives: [coreDirectives, forms.formDirectives]
)

class Notification {

  @Input()
  String color;
  
  @Input()
  bool isHighlighted;

  @Input()
  String text;
  
  @Input()
  dynamic event;

  final _showNotification = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get showNotification => _showNotification.stream;

  Notification()
  {

  }
  
  void clickShowNotification(dynamic e) {
    e.preventDefault();
  	_showNotification.add({
  		'event':event
  	});
  }

  String getColor() {
  	return "#${color}";
  }
  
  String getHighlight() {
  	if(isHighlighted) {
  		return '#cfdaf3';
  	}
  	return '#ffffff';
  }

}
