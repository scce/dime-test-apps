import 'package:angular/angular.dart';
import 'dart:js' as js;
import 'dart:async';

@Component(
    selector: 'DefaultImage',
    templateUrl: 'default_image.html',
	directives: [coreDirectives]
)
class DefaultImage implements OnInit, OnChanges {

  @Input()
  String first;
	
  @Input()
  String second;
	
  @Input('color')
  String inColor;
	
  @Input('size')
  int inSize;
	
  String text = "??";
  String color = "#777";
  String size = "35px";
  String fontSize = "14px";
  String borderWidth = "2px";

  @override
  ngOnInit() {
    update();
  }
  
  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    update();
  }
	
  void update() {
    text = toUpper(first) + toUpper(second);
    if (inColor?.isNotEmpty ?? false) {
      color = inColor;
	}
	if (inSize != null) {
      size = "${inSize}px";
      fontSize = "${(inSize * 0.4).floor()}px";
      borderWidth = "${(inSize * 0.05).ceil()}px";
	}
  }
  
  String toUpper(String s) {
    return (s?.isEmpty ?? true) ? "" : s[0].toUpperCase();
  }
}
