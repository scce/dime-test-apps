/*
 * Angular
 */
import 'package:angular/angular.dart';

import 'dart:js' as js;
import 'dart:async';


@Component(
    selector: 'LandingPage',
    templateUrl: 'landing_page.html',
	directives: [coreDirectives]
)
class LandingPage implements OnInit {


  // branch showIntent declaration
  @Output('login') Stream<Map<String,dynamic>> get evt_login => login.stream;
  StreamController<Map<String,dynamic>> login = new StreamController<Map<String,dynamic>>();
  
  // branch showIntent declaration
  @Output('register') Stream<Map<String,dynamic>> get evt_register => register.stream;
  StreamController<Map<String,dynamic>> register = new StreamController<Map<String,dynamic>>();

  @Output('terms') Stream<Map<String,dynamic>> get evt_terms => terms.stream;
  StreamController<Map<String,dynamic>> terms = new StreamController<Map<String,dynamic>>();
  
  @Output('legal') Stream<Map<String,dynamic>> get evt_legal => legal.stream;
  StreamController<Map<String,dynamic>> legal = new StreamController<Map<String,dynamic>>();
  
  @Output('privacy') Stream<Map<String,dynamic>> get evt_privacy => privacy.stream;
  StreamController<Map<String,dynamic>> privacy = new StreamController<Map<String,dynamic>>();
  
  @Output('accessibility') Stream<Map<String,dynamic>> get evt_accessibility => accessibility.stream;
  StreamController<Map<String,dynamic>> accessibility = new StreamController<Map<String,dynamic>>();

  LandingPage() {

  }

  @override
  ngOnInit() {
    js.context.callMethod("page_frame_init",[]);
    js.context.callMethod("landing_page_scrollTo",['page-top']);
  }
  
  void toTermsAndConditions(dynamic e) {
   e.preventDefault();
    terms.add(new Map());
  }
  
  void toLegal(dynamic e) {
   e.preventDefault();
    legal.add(new Map());
  }
  
  void toAccessibility(dynamic e) {
   e.preventDefault();
    accessibility.add(new Map());
  }
  
  void toPrivacy(dynamic e) {
   e.preventDefault();
    privacy.add(new Map());
  }

  void clickLogin(dynamic e){
    e.preventDefault();
    login.add(new Map());
  }
  
  void clickRegister(dynamic e){
    e.preventDefault();
    register.add(new Map());
  }
  
  void scrollTo(dynamic e,String id) {
  	e.preventDefault();
  	js.context.callMethod("landing_page_scrollTo",[id]);
  }
}
