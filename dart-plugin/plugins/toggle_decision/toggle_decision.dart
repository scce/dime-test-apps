import 'package:angular/angular.dart';
import 'dart:async';
import 'dart:html';

@Component(
    selector: 'ToggleDecision',
    templateUrl: 'toggle_decision.html',
    styleUrls: const ["toggle_decision.css"],
    directives: [coreDirectives]
)
class ToggleDecision {

  @Input() String status;

  @Input() dynamic user;

  final StreamController<Map<String,dynamic>> _acceptStream = new StreamController();
  @Output('switchAccept') Stream<Map<String,dynamic>> get acceptStream => _acceptStream.stream;
  
  final StreamController<Map<String,dynamic>> _rejectStream = new StreamController();
  @Output('switchReject') Stream<Map<String,dynamic>> get rejectStream => _rejectStream.stream;
  
  final StreamController<Map<String,dynamic>> _undoStream = new StreamController();
  @Output('switchUndo') Stream<Map<String,dynamic>> get undoStream => _undoStream.stream;

  void switchAccept(dynamic e){
  	if (status == "accept") {
  		status = "";
  		print('Toggle Decision: $status');
  		_undoStream.add({
  			'user':user
  		});
  	} else {
	    status = "accept";
	    print('Toggle Decision: $status');
	    _acceptStream.add({
	      'user':user
	    });
    }
  }
  
  void switchReject(dynamic e){
  	if (status == "reject") {
  		status = "";
  		print('Toggle Decision: $status');
  		_undoStream.add({
  			'user':user
  		});
  	} else {
	    status = "reject";
	    print('Toggle Decision: $status');
	    _rejectStream.add({
	      'user':user
	    });
    }
  }
  
  String getAcceptClass() {
    String cls = "toggle-button-accept";
    if (status == "accept") {
      cls += "-active";
    }
    return cls;
  }
  
  String getRejectClass() {
    String cls = "toggle-button-reject";
    if (status == "reject") {
      cls += "-active";
    }
    return cls;
  }
}
