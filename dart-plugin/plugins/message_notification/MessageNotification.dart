import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';

@Component(
  selector: "MessageNotification",
  templateUrl: 'message_notification.html',
  encapsulation: ViewEncapsulation.None,
  directives: [coreDirectives,forms.formDirectives]
)

class MessageNotification {

  @Input()
  String firstname;

  @Input()
  String lastname;

  @Input()
  dynamic message;

  @Input()
  DateTime time;

  @Input()
  String text;

  final _showChat = new StreamController<dynamic>();
  @Output() Stream<dynamic> get showChat => _showChat.stream;

  MessageNotification()
  {

  }
  
  void clickShowChat(dynamic e) {
  	_showChat.add({
  		'message':message
  	});
  }

  String getMessage() {
  	if(text.length>25){
  		return "${text.substring(25)}...";
  	}
  	return text;
  }

  String getTime() {
    if(time==null){
      return '';
    }
    var diff = new DateTime.now().difference(time);
    if(diff.inDays>0){
    		if(diff.inDays>30) {
    			return "${diff.inDays~/30} months";
    		}
    		if(diff.inDays>7) {
    			return "${diff.inDays~/7} weeks";
    		}
    		return "${diff.inDays} days";
    }
    if(diff.inHours>0) {
    		return "${diff.inHours} hours";
    	}
    	if(diff.inMinutes>0) {
    		return "${diff.inHours} minutes";
    	}
    	return "now";
  }
}
