import "dime-models/app.data" as data

dartPlugin affiliation_listing {
	styles ["plugins/affiliation_listing/affiliation_listing.css"]
	scripts ["plugins/affiliation_listing/affiliation_listing.dart"]
	template "plugins/affiliation_listing/affiliation_listing.html"
	component AffiliationListing {
		primitiveParameter "organization" : text
		primitiveParameter "department" : text
		primitiveParameter "street" : text
		primitiveParameter "zipCode" : text
		primitiveParameter "city" : text
		primitiveParameter "state" : text
		primitiveParameter "country" : text
	}
}

dartPlugin toggle_decision {
	styles ["plugins/toggle_decision/toggle_decision.css"]
	scripts ["plugins/toggle_decision/toggle_decision.dart"]
	template "plugins/toggle_decision/toggle_decision.html"
	component ToggleDecision {
		primitiveParameter "status" : text
		complexParameter "user" : data.ConcreteUser
		
		output switchAccept {
			complexParameter "user" : data.ConcreteUser
		}
		output switchReject {
			complexParameter "user" : data.ConcreteUser
		}
		output switchUndo {
			complexParameter "user" : data.ConcreteUser
		}
	}
}

dartPlugin toggle_decision_basic {
	styles ["plugins/toggle_decision_basic/toggle_decision_basic.css"]
	scripts ["plugins/toggle_decision_basic/toggle_decision_basic.dart"]
	template "plugins/toggle_decision_basic/toggle_decision_basic.html"
	component ToggleDecisionBasic {
		primitiveParameter "status" : text
		
		output switchAccept { }
		output switchReject { }
		output switchUndo { }
	}
}

dartPlugin toggle_switch_user {
	styles ["plugins/toggle_switch/toggle_switch.css"]
	scripts ["plugins/toggle_switch/toggle_switch_user.dart"]
	template "plugins/toggle_switch/toggle_switch.html"
	component ToggleSwitchUser {
		primitiveParameter "onText" : text
		primitiveParameter "offText" : text
		primitiveParameter "status" : boolean
		primitiveParameter "disabled" : boolean
		primitiveParameter "disabledMessage" : text
		complexParameter "object" : data.ConcreteUser
		
		output switchedOn {
			complexParameter "object" : data.ConcreteUser
		}
		output switchedOff {
			complexParameter "object" : data.ConcreteUser
		}
	}
}

dartPlugin accept_checkbox {
	scripts ["plugins/accept_checkbox/accept_checkbox.dart"]
	template "plugins/accept_checkbox/accept_checkbox.html"
	placeholder [label]
	component AcceptCheckbox {
		primitiveParameter "accept":boolean sync
	}
}

dartPlugin external_page_frame {
	styles ["plugins/external_page_frame/external_page_frame.css"]
	scripts ["plugins/external_page_frame/ExternalPageFrame.dart"]
	template "plugins/external_page_frame/external_page_frame.html"
	placeholder [content]
	component ExternalPageFrame {
		primitiveParameter "maxWidth": integer
		output back {}
		output terms {}
		output privacy {}
		output legal {}
		output accessibility {}
	}
}

dartPlugin internal_page_frame {
	styles ["plugins/internal_page_frame/internal_page_frame.css"]
	scripts ["plugins/internal_page_frame/internal_page_frame.dart"]
	template "plugins/internal_page_frame/internal_page_frame.html"
	placeholder [
		messages,notifications,usericon,content,conferenceicon,largeusericon,submitables
	]
	component InternalPageFrame {
		primitiveParameter "firstname":text
		primitiveParameter "lastname":text
		primitiveParameter "username":text
		complexParameter "user":data.ConcreteUser
		primitiveParameter "userSince":timestamp
		primitiveParameter "selectedConference":text
		complexParameter "conferencePhase":data.Phase
		complexParameter "conference":data.ConferenceService
		primitiveParameter "canConfigure":boolean
		primitiveParameter "canOverview":boolean
		primitiveParameter "canBid":boolean
		primitiveParameter "canAssign":boolean
		primitiveParameter "canReview":boolean
		primitiveParameter "canDiscuss":boolean
		primitiveParameter "canDecide":boolean
		primitiveParameter "canProduce":boolean
		primitiveParameter "countConfigure":integer
		primitiveParameter "countOverview":integer
		primitiveParameter "countBid":integer
		primitiveParameter "countAssign":integer
		primitiveParameter "countReview":integer
		primitiveParameter "countDiscuss":integer
		primitiveParameter "countDecide":integer
		primitiveParameter "countProduce":integer
		primitiveParameter "textConfigure":text
		primitiveParameter "textOverview":text
		primitiveParameter "textBid":text
		primitiveParameter "textAssign":text
		primitiveParameter "textReview":text
		primitiveParameter "textDiscuss":text
		primitiveParameter "textDecide":text
		primitiveParameter "textProduce":text
		primitiveParameter "conferenceColor":text
		primitiveParameter "messagesNotifications":integer
		primitiveParameter "newsNotifications":integer
		primitiveParameter "countNotifications":integer
		
		output legal{
		}
		
		output terms{
		}
		
		output privacy{
		}
		
		output accessibility{
		}
		
		output showMessages{
			complexParameter "user" :data.ConcreteUser
		}
		output showNotifications{
			complexParameter "user" :data.ConcreteUser
		}
		output showProfile{
			complexParameter "user" :data.ConcreteUser
		}
		output showArchive{
			complexParameter "user" :data.ConcreteUser
		}
		output showConference {
			complexParameter "conference":data.ConferenceService
		}
		output switchConference {
			complexParameter "user" :data.ConcreteUser
		}
		output showConferences {
			complexParameter "user" :data.ConcreteUser
		}
		output showIntent {
			primitiveParameter "intent":text
			complexParameter "conference":data.ConferenceService
		}
	}
}

dartPlugin default_image {
	styles ["plugins/default_image/default_image.css"]
	scripts ["plugins/default_image/DefaultImage.dart"]
	template "plugins/default_image/default_image.html"
	component DefaultImage {
		primitiveParameter "first":text
		primitiveParameter "second":text
		primitiveParameter "color":text
		primitiveParameter "size":integer
	}
}

dartPlugin hierarchical_property_field {
	styles ["plugins/hierarchical_property_field/hierarchical_property_field.css"]
	scripts ["plugins/hierarchical_property_field/hierarchical_property_field.dart"]
	template "plugins/hierarchical_property_field/hierarchical_property_field.html" placeholder [parent,value]
	component HierarchicalPropertyField {
		primitiveParameter "propertyName" : text
		primitiveParameter "inlineParentValue" : boolean
		primitiveParameter "inlineCustomValue" : boolean
		primitiveParameter "parentName" : text
		primitiveParameter "isInherited" : boolean sync
		complexParameter "parent" : data.Property
	}
}

dartPlugin notifier_component {
    styles ["plugins/notifier/notifier_component.css"]
	scripts ["plugins/notifier/notifier_component.dart"]
	template "plugins/notifier/notifier_component.html" placeholder [notifiercontent]
	component NotifierComponent {
		primitiveParameter "delay":integer
		primitiveParameter "type":text
		primitiveParameter "showOnStart":boolean

		event show {
		}
	}
}

dartPlugin workload_state {
	scripts ["plugins/workload_state/workload_state.dart"]
	template "plugins/workload_state/workload_state.html"
	component WorkloadState {
		primitiveParameter "workload" : integer
		primitiveParameter "warnIndex" : integer
		primitiveParameter "width" : integer
	}
}

dartPlugin keyword_list {
	styles ["plugins/keyword_list/keyword_list.css"]
	scripts ["plugins/keyword_list/keyword_list.dart"]
	template "plugins/keyword_list/keyword_list.html"
	
	component KeywordList {
		primitiveParameter "deletable" : boolean
		primitiveParameter "keywords" : text[]
		output removeKeyword {
			primitiveParameter "index" : integer
		}
	}
}

dartPlugin assignment_state {
	styles ["plugins/assignment_state/assignment_state.css"]
	scripts ["plugins/assignment_state/assignment_state.dart"]
	template "plugins/assignment_state/assignment_state.html"
	component AssignmentState {
		primitiveParameter "minAssignees" : integer
		primitiveParameter "numAssignees" : integer
		primitiveParameter "isFrozen" : boolean
	}
}

dartPlugin conference_structure {
	styles ["plugins/conference_structure/conference_tree.css"]
	scripts ["plugins/conference_structure/ConferenceTree.dart"]
	template "plugins/conference_structure/conference_tree.html"
	component ConferenceTree {
		complexParameter "rootNode":data.ConferenceService
		
		output conferenceServiceSelected {
			complexParameter "node":data.ConferenceService
		}
	}
}

dartPlugin toggle_button {
	styles ["plugins/toggle_button/toggle_button.css"]
	scripts ["plugins/toggle_button/toggle_button.dart"]
	template "plugins/toggle_button/toggle_button.html"
	component ToggleButton {
		primitiveParameter "onText" : text
		primitiveParameter "offText" : text
		primitiveParameter "value" : boolean sync
	}
}

dartPlugin decide_dropdown {
	styles ["plugins/decide_dropdown/decide_dropdown.css"]
	scripts ["plugins/decide_dropdown/decide_dropdown.dart"]
	template "plugins/decide_dropdown/decide_dropdown.html"
	
	component DecideDropdown {
		complexParameter "evaluation" : data.Evaluation
		complexParameter "paper" : data.Paper
		output setAccept {
			complexParameter "paper" : data.Paper
		}
		output setWeakAccept {
			complexParameter "paper" : data.Paper
		}
		output setIndifferent {
			complexParameter "paper" : data.Paper
		}
		output setWeakReject {
			complexParameter "paper" : data.Paper
		}
		output setReject {
			complexParameter "paper" : data.Paper
		}
	}
}

dartPlugin paperStatus {
	styles ["plugins/paper_status/paper_status.css"]
	scripts ["plugins/paper_status/paper_status.dart"]
	template "plugins/paper_status/paper_status.html" placeholder [submitted,assigned,reviewed,accepted,final,rejected]
	component PaperStatus {
		primitiveParameter "isSubmitted" : boolean
		primitiveParameter "isAssigned" : boolean
		primitiveParameter "isReviewed" : boolean
		primitiveParameter "isAccepted" : boolean
		primitiveParameter "isRejected" : boolean
		primitiveParameter "isFinal" : boolean
		primitiveParameter "paperId" : text
	}
}

dartPlugin reportStars {
	styles ["plugins/report_stars/report_stars.css"]
	scripts ["plugins/report_stars/report_stars.dart"]
	template "plugins/report_stars/report_stars.html"
	component ReportStars {
		primitiveParameter "score" : real
		primitiveParameter "max" : integer
		primitiveParameter "finished" : boolean
	}
}


dartPlugin review_state {
	styles ["plugins/review_state/review_state.css"]
	scripts ["plugins/review_state/review_state.dart"]
	template "plugins/review_state/review_state.html"
	component ReviewState {
		primitiveParameter "state" : text
		}
}
dartPlugin freeze_assignment_options {
	styles ["plugins/freeze_assignment_options/freeze_assignment_options.css"]
	scripts ["plugins/freeze_assignment_options/freeze_assignment_options.dart"]
	template "plugins/freeze_assignment_options/freeze_assignment_options.html"
	component FreezeAssignmentOptions {
		output selectionChanged {
			primitiveParameter "custom": boolean
			primitiveParameter "minAssignees": integer
		}

	}
}

dartPlugin landing_page {
	styles ["plugins/landing_page/landing_page.css"]
	scripts ["plugins/landing_page/LandingPage.dart"]
	template "plugins/landing_page/landing_page.html"
	component LandingPage {
		
		output login {}
		output accessibility {}
		output terms {}
		output register {}
		output privacy {}
		output legal {}
	}
}

dartPlugin produceState {
	styles ["plugins/produce_state/produce_state.css"]
	scripts ["plugins/produce_state/produce_state.dart"]
	template "plugins/produce_state/produce_state.html"
	component ProduceState {
		primitiveParameter "paper" : boolean
		primitiveParameter "archive" : boolean
		primitiveParameter "consent" : boolean
		primitiveParameter "registrant" : boolean
	}
}

dartPlugin stackedProgressBar {
  styles ["plugins/stacked_progress_bar/stacked_progress_bar.css"]
  scripts ["plugins/stacked_progress_bar/StackedProgressBar.dart"]
  template "plugins/stacked_progress_bar/stacked_progress_bar.html"
  component StackedProgressBar {
    primitiveParameter "greenCount" : integer
    primitiveParameter "greenName" : text
    primitiveParameter "greenTooltip" : text
    primitiveParameter "orangeCount" : integer
    primitiveParameter "orangeName" : text
    primitiveParameter "orangeTooltip" : text
    primitiveParameter "redCount" : integer
    primitiveParameter "redName" : text
    primitiveParameter "redTooltip" : text
    event update {
      primitiveParameter "greenCount" : integer
      primitiveParameter "orangeCount" : integer
      primitiveParameter "redCount" : integer
    }
    output clickedGreen{}
    output clickedOrange{}
    output clickedRed{}
  }
}


dartPlugin review_result {
	styles ["plugins/review_result/review_result.css"]
	scripts ["plugins/review_result/review_result.dart"]
	template "plugins/review_result/review_result.html"
	component ReviewResult {
		complexParameter "evaluation" : data.Evaluation
		complexParameter "confidence" : data.Confidence
	}
}

dartPlugin subreview_state {
	styles ["plugins/subreview_state/subreview_state.css"]
	scripts ["plugins/subreview_state/subreview_state.dart"]
	template "plugins/subreview_state/subreview_state.html"
	component SubreviewState {
		complexParameter "state" : data.InvitationStatus
	}
}

dartPlugin subreviewStatusBar {
  styles ["plugins/subreview_status_bar/subreview_status_bar.css"]
  scripts ["plugins/subreview_status_bar/SubreviewStatusBar.dart"]
  template "plugins/subreview_status_bar/subreview_status_bar.html"
  component SubreviewStatusBar {
    primitiveParameter "greenCount" : integer
    primitiveParameter "orangeCount" : integer
    primitiveParameter "redCount" : integer
    event update {
      primitiveParameter "greenCount" : integer
      primitiveParameter "orangeCount" : integer
      primitiveParameter "redCount" : integer
    }
    output clickedGreen{}
    output clickedOrange{}
    output clickedRed{}
  }
}

dartPlugin user_description {
	styles ["plugins/user_description/user_description.css"]
	scripts ["plugins/user_description/user_description.dart"]
	template "plugins/user_description/user_description.html"
	component UserDescription {
		primitiveParameter "name" : text
		primitiveParameter "affiliation" : text
		primitiveParameter "email" : text
	}
}

dartPlugin tooltip {
	scripts ["plugins/tooltip/TooltipAdvanced.dart"]
	template "plugins/tooltip/tooltip_advanced.html" placeholder [content,hover]
	component TooltipAdvanced {
		primitiveParameter "position":text
	}
}

dartPlugin vpat {
	styles ["plugins/vpat/vpat.css"]
	scripts ["plugins/vpat/vpat.dart"]
	template "plugins/vpat/vpat.html"
	component VPAT {}
}

dartPlugin page_frame_message {
	scripts ["plugins/message_notification/MessageNotification.dart"]
	template "plugins/message_notification/message_notification.html" placeholder [sendericon]
	component MessageNotification {
		primitiveParameter "firstname":text
		primitiveParameter "lastname":text
		complexParameter "message":data.ChatMessage
		primitiveParameter "time":timestamp
		primitiveParameter "text":text
		
		output showChat {
			complexParameter "message":data.ChatMessage
		}
	}
}

dartPlugin page_frame_notification {
	scripts ["plugins/notification/Notification.dart"]
	template "plugins/notification/notification.html"
	component Notification {
		primitiveParameter "color":text
		primitiveParameter "text":text
		primitiveParameter "isHighlighted":boolean
		complexParameter "event":data.EventEntry
		
		output showNotification {
			complexParameter "event":data.EventEntry
		}
	}
}

dartPlugin submitable_entry {
	scripts ["plugins/submitable_entry/SubmitableEntry.dart"]
	template "plugins/submitable_entry/submitable_entry.html"
	component SubmitableEntry {
		primitiveParameter "name":text
		complexParameter "manuscriptService":data.ManuscriptService
		
		output submitToTrack {
			complexParameter "manuscriptService":data.ManuscriptService
		}
	}
}

dartPlugin toggle_switch_paper {
	styles ["plugins/toggle_switch/toggle_switch.css"]
	scripts ["plugins/toggle_switch/toggle_switch_paper.dart"]
	template "plugins/toggle_switch/toggle_switch.html"
	component ToggleSwitchPaper {
		primitiveParameter "onText" : text
		primitiveParameter "offText" : text
		primitiveParameter "status" : boolean
		primitiveParameter "disabled" : boolean
		primitiveParameter "disabledMessage" : text
		complexParameter "object" : data.Paper
		
		output switchedOn {
			complexParameter "object" : data.Paper
		}
		output switchedOff {
			complexParameter "object" : data.Paper
		}
	}
}

dartPlugin toggle_switch_author {
	styles ["plugins/toggle_switch/toggle_switch.css"]
	scripts ["plugins/toggle_switch/toggle_switch_author.dart"]
	template "plugins/toggle_switch/toggle_switch.html"
	component ToggleSwitchAuthor {
		primitiveParameter "onText" : text
		primitiveParameter "offText" : text
		primitiveParameter "status" : boolean
		primitiveParameter "disabled" : boolean
		primitiveParameter "disabledMessage" : text
		complexParameter "object" : data.Author
		
		output switchedOn {
			complexParameter "object" : data.Author
		}
		output switchedOff {
			complexParameter "object" : data.Author
		}
	}
}

