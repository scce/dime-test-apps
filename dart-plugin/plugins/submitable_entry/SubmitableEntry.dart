import 'package:angular/angular.dart';
import 'dart:async';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
  selector: "SubmitableEntry",
  templateUrl: 'submitable_entry.html',
  encapsulation: ViewEncapsulation.None,
  directives: [coreDirectives, forms.formDirectives]
)

class SubmitableEntry {

  @Input()
  String name;
  
  @Input()
  dynamic manuscriptService;

  final _submitToTrack = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get submitToTrack => _submitToTrack.stream;
  
  void clickSubmitTrack(dynamic e) {
  	e.preventDefault();
  	_submitToTrack.add({
  		'manuscriptService':manuscriptService
  	});
  }

}
