import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
  selector: "ReportStars",
  templateUrl: 'report_stars.html',
  styleUrls: const ["report_stars.css"],
  pipes: const [DecimalPipe],
  directives: [coreDirectives, forms.formDirectives]
)

class ReportStars implements OnInit {
  
  	
  	@Input() double score;
  	@Input() int max;
  	@Input() bool finished;
  	
  	List<int> stars;
  	
	ReportStars()
	{
		stars = new List();
	}
	
	void ngOnInit()
	{
		stars = new List(max);
	}
	
	String getStarClass(int i)
	{
		String s = "ion-android-star";
		if((i+1)>score){
			var c = (i+1)-score;
			if(c > 0.75) {
				s+="-outline";
			}
			if(c <= 0.75 && c > 0.25) {
				s+="-half";
			}
		}
		if (!finished) {
			s += " star-disabled";
		}
		return s;
	}
	
	String getScoreClass() {
		if (!finished) {
			return "score-disabled";
		}
		return "";
	}
	
}
