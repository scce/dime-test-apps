import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
	selector: "SubreviewState",
	templateUrl: 'subreview_state.html',
	styleUrls: const ["subreview_state.css"],
	directives: [coreDirectives, forms.formDirectives]
)

class SubreviewState {
  
	@Input() dynamic state;
	
	String getStateClass() {
		return "";
	}
	
	String getIndicatorClass() {
		if (state == null) {
			return "subreview-state-indicator-invited";
		}
		return "subreview-state-indicator-${getValue(state)}";
	}
	
	String getTooltip() {
		if (this.state == null)
			return "invitation pending";
		String state = getValue(this.state);
		if (state == "invited")
			return "invitation pending";
		if (state == "accepted")
			return "invitation accepted - review pending";
		if (state == "declined")
			return "invitation declined";
		if (state == "finished")
			return "review finished - report available";
		return state;
	}
	
	String getValue(dynamic obj) {
		return obj.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "").toLowerCase();
	}
}
