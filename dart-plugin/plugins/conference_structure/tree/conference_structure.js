/**
 * Created by zweihoff on 08.11.17.
 */
var conference_structure_data = {};
var conference_structure_selection_cb = null;

function show_confernence_structure(
    selected_cb
) {
    conference_structure_data = {};
    conference_structure_selection_cb = null;
    conference_structure_selection_cb = selected_cb;
    d3.selectAll("svg > *").remove();
    var svg = d3.select("svg"),
        width = +svg.attr("width")-160,
        height = +svg.attr("height")-40,
        g = svg.append("g").attr("transform", "translate(80,0)");
            svg.attr("preserveAspectRatio", "xMinYMin");

    document.getElementById('conference_structure').onclick = function(){
        highlight(null);
        conference_structure_selection_cb(null);
    };

    var tree = d3.tree()
        .size([height-40, width - 160]);

    conference_structure_data.data = [];
    conference_structure_data.tree = tree;
    conference_structure_data.g = g;

}

function addNode(parentId,id,name,isSubmission,isCompound,color) {
    conference_structure_data.data[conference_structure_data.data.length] = {
        id:id,
        parentId:parentId,
        nodeName: name,
        isSubmission:isSubmission,
        isCompound:isCompound,
        color:color
    };

}

function highlight(node) {
    //unhighlight all
    d3.selectAll('g.node').classed("node-selected", false);  //here's how you get all the nodes

    //highlight
    if(node !== null) {
        d3.select(node).classed("node-selected", true);
    }

}


function renderTree() {
    var root = d3.stratify()
        .id(function(d) { return d.id; })
        .parentId(function(d) { return d.parentId; })(conference_structure_data.data);

    var link = conference_structure_data.g.selectAll(".link")
        .data(conference_structure_data.tree(root).links())
        .enter().append("path")
        .attr("class", "link")
        .attr("d", d3.linkHorizontal()
            .x(function(d) { return d.y; })
            .y(function(d) { return d.x; }));

    var node = conference_structure_data.g.selectAll(".node")
        .data(root.descendants())
        .enter().append("g")
        .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

    node.append("circle")
        .attr("r", 5)
        .style("fill", function(d) { return d.data.color});

    node.append("text")
        .attr("dy", 3)
        .attr("x", function(d) { return d.children ? -8 : 8; })
        .style("text-anchor", function(d) { return d.children ? "end" : "start"; })
        .style("cursor", "pointer")
        .text(function(d) {
            return d.data.nodeName;
        });
    node.filter(function(d){ return d.data.isSubmission||d.data.isCompound; })
        .append("text")
        .attr("dy", 14)
        .attr("x", function(d) { return d.children ? -8 : 8; })
        .attr("class","subtitle")
        .style("text-anchor", function(d) { return d.children ? "end" : "start"; })
        .style("cursor", "pointer")
        .text(function(d) {
            return (d.data.isSubmission?"Submission":"")+" "+(d.data.isCompound?"Compound":"");
        });
    node.on('click',function(d,evt){
        highlight(this);
        d3.event.stopPropagation();
        conference_structure_selection_cb(d.data.id);
    });
}



