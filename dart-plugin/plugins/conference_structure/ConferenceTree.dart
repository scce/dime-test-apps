/*
 * Angular
 */
import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:angular_forms/angular_forms.dart' as forms;

import 'dart:js' as js;
import 'dart:html';
import 'dart:async';

import 'package:app/src/models/Selectives.dart';

import 'package:app/src/data/Data.dart' as data;

@Component(
    selector: 'ConferenceTree',
    templateUrl: 'conference_tree.html',
    styleUrls: const ["conference_tree.css"],
    directives: [coreDirectives,forms.formDirectives]

)
class ConferenceTree implements OnInit, OnChanges {

  @Input('rootNode')
  dynamic treeRoot;

  @Output('conferenceServiceSelected') Stream<dynamic> get evt_conferenceServiceSelected => conferenceServiceSelected.stream;
  StreamController<dynamic> conferenceServiceSelected = new StreamController();


  List<data.ConferenceService> nodes = new List();

  List<data.ConferenceService> checkSubmissionsAreLeafsNodes = new List();
  List<data.ConferenceService> checkNoMultipleCompoundNodes = new List();
  List<data.ConferenceService> checkSubmissionIsReachableNodes = new List();
  

  String renderNodeLabels(List<data.ConferenceService> nodes) => nodes.map((n)=>n.name).join(", ");

  ///
  /// Checks if at least one submitting node is present
  ///
  bool checkOneSubmission() => nodes.where((n)=>n is data.ManuscriptService).isNotEmpty;

  ///
  /// Checks if at least one node at all is present
  ///
  bool checkOneNode() => nodes.isNotEmpty;

  ///
  /// Checks if every submitting node is a leaf of the tree
  ///
  bool checkSubmissionsAreLeafs() {
    return true;
  }

  ///
  /// Checks if every submitting node can be reached by a compound
  ///
  bool checkOneCompound() => nodes.where((n)=>n is data.ConferenceServicePart).where((n)=>n.isCompound).isNotEmpty;

  ///
  /// Checks if every compound has no other compound above it
  ///
  bool checkNoMultipleCompound() {
    var result = nodes.where((n)=>n is data.ConferenceServicePart).where((n)=>n.isCompound).where((n)=>_checkPathToRoot(n)==false);
    checkNoMultipleCompoundNodes.clear();
    checkNoMultipleCompoundNodes.addAll(result);
    return result.isEmpty;
  }

  ///
  /// Checks if every submitting node can be reached from a compound node
  ///
  bool checkSubmissionIsReachable() {
    var result = nodes.where((n)=>n is data.ManuscriptService).where((n)=>_checkPathToAllSubmissions(n)==false);
    checkSubmissionIsReachableNodes.clear();
    checkSubmissionIsReachableNodes.addAll(result);
    return result.isEmpty;
  }

  ///
  /// Checks if the node is compound or one if its parents
  ///
  bool _checkPathToAllSubmissions(data.ConferenceService node) {
    if(node.isCompound){
      return true;
    }
    //find parent
    var parent = nodes.where((n)=>n is data.ConferenceServicePart).where((n)=>n.children.where((child)=>child.dywa_id==node.dywa_id).isNotEmpty);
    if(parent.isEmpty){
      //is root
      return false;
    }
    return _checkPathToAllSubmissions(parent.first);
  }

  ///
  /// Check if every no compound node can be found in one of the nodes parents
  ///
  bool _checkPathToRoot(data.ConferenceService node) {
    //find parent
    var parent = nodes.where((n)=>n is data.ConferenceServicePart).where((n)=>n.children.where((child)=>child.dywa_id==node.dywa_id).isNotEmpty);
    if(parent.isEmpty){
      //is root
      return true;
    }
    if(parent.first.isCompound) {
      return false;
    }
    return _checkPathToRoot(parent.first);
  }

  @override
  ngOnInit() {
    //collect tree nodes
    nodes.clear();
    //build d3
    js.context.callMethod("show_confernence_structure",[selectionChanged]);
    //add nodes
    if(treeRoot!=null){
      window.console.log("oninit");
      window.console.log(treeRoot);
      renderNode(null,treeRoot);
    }
    //render tree
    js.context.callMethod("renderTree",[]);


  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    if(changes.containsKey("treeRoot")){
      nodes.clear();
      js.context.callMethod("show_confernence_structure",[selectionChanged]);
        //rebuild tree
      if(treeRoot!=null) {
        window.console.log("onchange");
        renderNode(null,treeRoot);
      }

      js.context.callMethod("renderTree",[]);
    }
  }

  void renderNode(data.ConferenceServicePart parent,data.ConferenceService me) {
    window.console.log(me);
    nodes.add(me);
    js.context.callMethod("addNode",[
      parent==null?"":parent.dywa_id,
      me.dywa_id,
      me.name,
      me is data.ManuscriptService,
      me.isCompound,
      me.color
    ]);
    if(me is data.ConferenceServicePart){
      me.children.forEach((n)=>renderNode(me,n));
    }
  }

  void selectionChanged(id) {
  	if(id == null) {
  		return;
  	}
    print(id);
    var result = nodes.where((n)=>n.dywa_id==id);
    if(result.isNotEmpty) {
	    conferenceServiceSelected.add({
	    		'node':result.first
	    });    
    }
  }


}
