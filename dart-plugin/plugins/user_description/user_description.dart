import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
    selector: 'UserDescription',
    templateUrl: 'user_description.html',
    styleUrls: const ["user_description.css"],
    directives: [coreDirectives, forms.formDirectives]
)

class UserDescription {

  @Input()
  String name;
  String getName() => name ?? "";

  @Input()
  String affiliation;
  String getAffiliation() => affiliation ?? "no affiliation";

  @Input()
  String email;
  String getEmail() => email ?? "no email";
  
  bool isEmpty(String val) => val?.isEmpty;
}
