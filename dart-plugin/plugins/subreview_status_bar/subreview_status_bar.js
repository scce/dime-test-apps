function makeSubreviewStatusBarsSticky() {
  $('.subreview-status-bar').each(function() {
    if (typeof $(this) !== "undefined") {
      var offset = $(this).offset();
      if (typeof offset !== "undefined") {
        var top = offset.top;
        var win = $(window);
        var subreviewStatusBar = $(this);
        win.on("scroll", function() {
          win.scrollTop() >= top
          ? subreviewStatusBar.addClass("keep-me-fixed")
          : subreviewStatusBar.removeClass("keep-me-fixed");
        });
      }
    }
  });
}

