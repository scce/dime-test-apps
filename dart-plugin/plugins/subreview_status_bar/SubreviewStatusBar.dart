import 'package:angular/angular.dart';
import 'dart:async';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
    selector: 'SubreviewStatusBar',
    templateUrl: 'subreview_status_bar.html',
    styleUrls: const ["subreview_status_bar.css"],
    directives: [coreDirectives, forms.formDirectives]
)

class SubreviewStatusBar implements OnInit {
  
  @Input('greenCount')
  num greenCount;
  num getGreenCount() => greenCount ?? 0;
  
  String greenName;
  String getGreenName() => greenName ?? '';
  
  String greenTooltip;
  String getGreenTooltip() => greenTooltip ?? '';
  
  @Input('orangeCount')
  num orangeCount;
  num getOrangeCount() => orangeCount ?? 0;
  
  String orangeName;
  String getOrangeName() => orangeName ?? '';
  
  String orangeTooltip;
  String getOrangeTooltip() => orangeTooltip ?? '';
  
  @Input('redCount')
  num redCount;
  num getRedCount() => redCount ?? 0;
  
  String redName;
  String getRedName() => redName ?? '';
  
  String redTooltip;
  String getRedTooltip() => redTooltip ?? '';
  
  final _clickedGreen = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedGreen => _clickedGreen.stream;
  
  final _clickedOrange = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedOrange => _clickedOrange.stream;
  
  final _clickedRed = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedRed => _clickedRed.stream;
  
  String greenPercentage, orangePercentage, redPercentage;
  
  @override
  void ngOnInit() {
    greenName = "Submitted";
    greenTooltip = "Amount of submitted subreviews";
    orangeName = "Invite accepted";
    orangeTooltip = "Amount of accepted invites to subreviews without submission";
    redName = "Invited";
    redTooltip = "Amount of invited subreviewers without response yet";
      
    updatePercentage();
  }
  
  void triggerGreen() {
    _clickedGreen.add({});
  }
  
  void triggerOrange() {
    _clickedOrange.add({});
  }
  
  void triggerRed() {
  	
    _clickedRed.add({});
  }
  
  void update(int greenCount, int orangeCount, int redCount) {
  	this.greenCount = greenCount;
  	this.orangeCount = orangeCount;
  	this.redCount = redCount;
  	updatePercentage();
  }
  
  void updatePercentage() {
    var countSum = getGreenCount() + getOrangeCount() + getRedCount();
    greenPercentage = (100 / countSum * getGreenCount()).toString() + '%';
    orangePercentage = (100 / countSum * getOrangeCount()).toString() + '%';
    redPercentage = (100 / countSum * getRedCount()).toString() + '%';
  }
}
