import 'package:angular/angular.dart';
import 'package:angular/core.dart';

@Component(
    selector: 'AffiliationListing',
    templateUrl: 'affiliation_listing.html',
    styleUrls: const ['affiliation_listing.css'],
    directives: [coreDirectives]
)

class AffiliationListing {

  @Input('organization')
  String organization;
  String getOrganization() => organization ?? "";

  @Input('department')
  String department;
  String getDepartment() => department ?? "";

  @Input('street')
  String street;
  String getStreet() => street ?? "";

  @Input('zipCode')
  String zipCode;
  String getZipCode() => zipCode ?? "";

  @Input('city')
  String city;
  String getCity() => city ?? "";

  @Input('state')
  String state;
  String getState() => state ?? "";

  @Input('country')
  String country;
  String getCountry() => country ?? "";
  
  bool isEmpty(String val) => val?.isEmpty;
}
