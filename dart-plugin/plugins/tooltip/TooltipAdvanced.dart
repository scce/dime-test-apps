/*
 * Angular
 */
import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:html' as html;
import 'dart:math' as math;
import 'dart:async';


@Component(
  selector: 'TooltipAdvanced',
  templateUrl: 'tooltip_advanced.html',
  directives: [coreDirectives, forms.formDirectives]

)
class TooltipAdvanced {

  @Input()
  String position;

  bool show = false;
  bool overTooltip = false;
  bool overContent = false;

  @ViewChild('tooltipatrget') ElementRef tooltipatrget;

  @ViewChild('tooltipsource') ElementRef tooltipsource;

  String top = "-9999px";
  String left = "-9999px";
  String bottom = "auto";
  String right = "auto";

  TooltipAdvanced() {

  }

  void leaveTooltip(e) {
    e.preventDefault();
    overTooltip=false;
    hideTooltip(e);
  }

  void keepTooltip(e) {
    e.preventDefault();
    overTooltip=true;
  }

  void hideTooltip(dynamic e){
    overTooltip=false;
    overContent=false;
    e.preventDefault();
    new Timer(new Duration(microseconds: 5),(){
      if(!overTooltip&&!overContent){
        top = "-9999px";
        left = "-9999px";
        bottom = "auto";
        right = "auto";
      }
    });

  }

  String getPositionClass() => position==null?'top':position.toLowerCase();

  int getWidth(html.Element e){
    if(e.clientWidth<=0){
      return e.childNodes.where((n)=>n is html.Element).map((n)=>n as html.Element).map((n)=>getWidth(n)).reduce((a,b)=>a+b);
    }
    return 0;
  }

  void showTooltip(html.MouseEvent e){
    overContent = true;

    if(position==null) {
      position = "top";
    }
      switch(position.toLowerCase()){


        case "left":{
          top = "-50%";
          left = "auto";
          bottom = "auto";
          right = "101%";
        }break;
        case "right":{
          top = "-50%";
          left = "101%";
          bottom = "auto";
          right = "auto";
        }break;
        case "bottom":{
          top = "100%";
          left = "0";
          bottom = "auto";
          right = "auto";
        }break;
        default:{
          top =  "auto";
          left =  "0";
          bottom = "100%";
        }
      }


  }

}
