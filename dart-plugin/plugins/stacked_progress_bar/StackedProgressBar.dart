import 'package:angular/angular.dart';
import 'dart:async';
import 'dart:collection';

@Component(
    selector: 'StackedProgressBar',
    templateUrl: 'stacked_progress_bar.html',
    styleUrls: const ["stacked_progress_bar.css"],
    directives: [coreDirectives]
)

class StackedProgressBar implements OnInit {
  
  @Input('greenCount')
  num greenCount;
  num getGreenCount() => greenCount ?? 0;
  
  @Input('greenName')
  String greenName;
  String getGreenName() => greenName ?? '';
  
  @Input('greenTooltip')
  String greenTooltip;
  String getGreenTooltip() => greenTooltip ?? '';
  
  @Input('orangeCount')
  num orangeCount;
  num getOrangeCount() => orangeCount ?? 0;
  
  @Input('orangeName')
  String orangeName;
  String getOrangeName() => orangeName ?? '';
  
  @Input('orangeTooltip')
  String orangeTooltip;
  String getOrangeTooltip() => orangeTooltip ?? '';
  
  @Input('redCount')
  num redCount;
  num getRedCount() => redCount ?? 0;
  
  @Input('redName')
  String redName;
  String getRedName() => redName ?? '';
  
  @Input('redTooltip')
  String redTooltip;
  String getRedTooltip() => redTooltip ?? '';
  
  final _clickedGreen = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedGreen => _clickedGreen.stream;
  
  final _clickedOrange = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedOrange => _clickedOrange.stream;
  
  final _clickedRed = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get clickedRed => _clickedRed.stream;
  
  String greenPercentage, orangePercentage, redPercentage;
  
  @override
  void ngOnInit() {
    updatePercentage();
  }
  
  void triggerGreen() {
    _clickedGreen.add(new Map());
  }
  
  void triggerOrange() {
    _clickedOrange.add(new Map());
  }
  
  void triggerRed() {
    _clickedRed.add(new Map());
  }
  
  void update(int greenCount, int orangeCount, int redCount) {
  	this.greenCount = greenCount;
  	this.orangeCount = orangeCount;
  	this.redCount = redCount;
  	updatePercentage();
  }
  
  void updatePercentage() {
    var countSum = getGreenCount() + getOrangeCount() + getRedCount();
    greenPercentage = (100 / countSum * getGreenCount()).toString() + '%';
    orangePercentage = (100 / countSum * getOrangeCount()).toString() + '%';
    redPercentage = (100 / countSum * getRedCount()).toString() + '%';
  }
}
