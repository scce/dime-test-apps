function makeStackedProgressBarsSticky() {
  $('.stacked-progress-bar').each(function() {
    if (typeof $(this) !== "undefined") {
      var offset = $(this).offset();
      if (typeof offset !== "undefined") {
        var top = offset.top;
        var win = $(window);
        var stackedProgressBar = $(this);
        win.on("scroll", function() {
          win.scrollTop() >= top
          ? stackedProgressBar.addClass("keep-me-fixed")
          : stackedProgressBar.removeClass("keep-me-fixed");
        });
      }
    }
  });
}

