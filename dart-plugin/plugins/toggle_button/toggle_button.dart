import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';

@Component(
    selector: 'ToggleButton',
    templateUrl: 'toggle_button.html',
    styleUrls: const ["toggle_button.css"],
    directives: [coreDirectives, forms.formDirectives]
)
class ToggleButton implements OnInit {
  
  @Input('onText')
  String onText = "on";

  @Input('offText')
  String offText = "off";
  
  @Input('value') bool value;

  @Output('syncvalue') Stream<Map<String,dynamic>> get evt_syncvalue => syncvaluestream.stream;
  StreamController<Map<String,dynamic>> syncvaluestream = new StreamController();

  @override
  ngOnInit() {
    if (offText == null) {
      offText = "off";
    }
    if (onText == null) {
      onText = "on";
    }
  }

  void switchOn(dynamic e){
  	value = true;
  	syncvaluestream.add({ 'value':value });
  }
  
  void switchOff(dynamic e){
  	value = false;
  	syncvaluestream.add({ 'value':value });
  }
  
  String getOnClass() {
    String cls = "toggle-button-on";
    if (value) {
      cls += "-active";
    }
    return cls;
  }
  
  String getOffClass() {
    String cls = "toggle-button-off";
    if (!value) {
      cls += "-active";
    }
    return cls;
  }
}
