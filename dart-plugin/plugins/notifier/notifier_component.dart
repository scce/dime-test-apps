import 'package:angular/angular.dart';
import 'dart:async';

@Component(
    selector: 'NotifierComponent',
    templateUrl: 'notifier_component.html',
    styleUrls: const ['notifier_component.css'],
    directives: [coreDirectives]
)
class NotifierComponent implements OnInit {

  @Input()
  int delay;

  @Input()
  String type;

  @Input()
  bool showOnStart = true;

  String animation = "dime-alter-show";


  bool visible = false;

  final ChangeDetectorRef ref;

  NotifierComponent(ChangeDetectorRef this.ref){

  }


  @override
  void ngOnInit()
  {
    if(showOnStart){
      show();
    }
  }

  void show()
  {
    animation = "dime-alter-show";
    visible = true;
    print("show notifier ${visible}");
    ref.detectChanges();
    close();
  }

  String getClass()
  {
    return "alert alert-${type} ${animation}";
  }

  void close()
  {
    if(delay>0) {
      print("delay");
      new Timer(new Duration(seconds: delay),hide);
    }

  }

  void hide()
  {
    print("hide");
    animation = "dime-alter-hidden";
    new Timer(new Duration(seconds: 1),(){
      visible = false;
    });


  }

}
