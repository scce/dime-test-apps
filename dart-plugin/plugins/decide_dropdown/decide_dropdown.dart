import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';
import 'dart:html';

@Component(
    selector: 'DecideDropdown',
    templateUrl: 'decide_dropdown.html',
    styleUrls: const ["decide_dropdown.css"],
    directives: [coreDirectives, forms.formDirectives]
)

class DecideDropdown {

  @Input() dynamic evaluation;
  @Input() dynamic paper;
  
  final _setAccept = new StreamController<dynamic>();
  @Output() Stream<dynamic> get setAccept => _setAccept.stream;
  
  final _setWeakAccept = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get setWeakAccept => _setWeakAccept.stream;
  
  final _setIndifferent = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get setIndifferent => _setIndifferent.stream;
  
  final _setWeakReject = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get setWeakReject => _setWeakReject.stream;
  
  final _setReject = new StreamController<Map<String,dynamic>>();
  @Output() Stream<Map<String,dynamic>> get setReject => _setReject.stream;
  
  String getToggleLabel() {
    if (evaluation == null) {
      return "";
    }
    return "${evaluation}";
  }
  
  void buttonAcceptClicked(dynamic evt) { 
    evt.preventDefault();
    print('button Accept clicked');
    _setAccept.add({
    	'paper':paper
    });
  }
  
  void buttonWaekAcceptClicked(dynamic evt) { 
    evt.preventDefault();
    print('button Weak Accept clicked');
    _setWeakAccept.add({
    	'paper':paper
    });
  }
  
  void buttonIndifferentClicked(dynamic evt) { 
    evt.preventDefault();
    print('button Indifferent clicked');
    _setIndifferent.add({
    	'paper':paper
    });
  }
  
  void buttonWeakRejectClicked(dynamic evt) { 
    evt.preventDefault();
    print('button WeakReject clicked');
    _setWeakReject.add({
    	'paper':paper
    });
  }
  
  void buttonRejectClicked(dynamic evt) { 
    evt.preventDefault();
    print('button Reject clicked');
    _setReject.add({
    	'paper':paper
    });
  }
}
