import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:js' as js;
import 'dart:async';


@Component(
    selector: 'ToggleSwitchAuthor',
    templateUrl: 'toggle_switch.html',
    styleUrls: const ["toggle_switch.css"],
    directives: [coreDirectives, forms.formDirectives]

)
class ToggleSwitchAuthor implements OnInit {

  @Input('onText')
  String onText = "on";

  @Input('offText')
  String offText = "off";

  @Input('status')
  bool status = false;
  
  @Input('disabled')
  bool disabled = false;
  
  @Input('disabledMessage')
  String disabledMessage = "Disabled";
  
  @Input('object')
  dynamic object;

  // branch showIntent declaration
  @Output('switchedOn') Stream<Map<String,dynamic>> get evt_on => onstream.stream;
  StreamController<Map<String,dynamic>> onstream = new StreamController();

  @Output('switchedOff') Stream<Map<String,dynamic>> get evt_off => offstream.stream;
  StreamController<Map<String,dynamic>> offstream = new StreamController();


  @override
  ngOnInit() {
    if(offText==null){
      offText = "off";
    }
    if(onText==null){
      onText = "on";
    }
  }

  void toggle(dynamic e){
    if (disabled)
      return;
    status=!status;
    print('Toggle status: $status');
    if(status){
      onstream.add({
      	'object':object
      });
    } else {
      offstream.add({
      	'object':object
      });
    }
  }
}
