import 'package:angular/angular.dart';
import 'dart:async';

@Component(
    selector: 'ToggleSwitch',
    templateUrl: 'toggle_switch.html',
    styleUrls: const ["toggle_switch.css"],
    directives: [coreDirectives]
)
class ToggleSwitch implements OnInit {

  @Input() String onText = "on";

  @Input() String offText = "off";

  @Input() bool status = false;
  
  @Input() bool disabled = false;
  
  @Input() String disabledMessage = "Disabled";
  
  @Input() dynamic object;

  final StreamController<Map<String,dynamic>> _onStream = new StreamController();
  @Output('switchedOn') Stream<Map<String,dynamic>> get onStream => _onStream.stream;

  final StreamController<Map<String,dynamic>> _offStream = new StreamController();
  @Output('switchedOff') Stream<Map<String,dynamic>> get offStream => _offStream.stream;

  @override
  ngOnInit() {
    if (offText == null) {
      offText = "off";
    }
    if (onText == null) {
      onText = "on";
    }
  }

  void toggle(dynamic e){
    if (disabled)
      return;
    status = !status;
    print('Toggle status: $status');
    if (status) {
      _onStream.add({
      	'object':object
      });
    } else {
      _offStream.add({
      	'object':object
      });
    }
  }
}
