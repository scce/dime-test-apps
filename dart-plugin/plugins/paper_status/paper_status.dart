import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';
import 'dart:html';

@Component(
  selector: "PaperStatus",
  templateUrl: 'paper_status.html',
  styleUrls: const ["paper_status.css"],
  directives: [coreDirectives, forms.formDirectives]
)

class PaperStatus {

  	@Input() bool isSubmitted;
  	@Input() bool isAssigned;
  	@Input() bool isReviewed;
  	@Input() bool isAccepted;
  	@Input() bool isRejected;
  	@Input() bool isFinal;
  	@Input() String paperId;
  	
  	void showModalSubmitted() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="submitted"] a[data-toggle="modal"]');
  		if(obj == null || !isSubmitted) {
  			return;
  		}
  		obj.click();
  	}
  	
  	void showModalAssigned() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="assigned"] a[data-toggle="modal"]');
  		if(obj == null || !isAssigned) {
  			return;
  		}
  		obj.click();
  	}
  	
  	void showModalReviewed() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="reviewed"] a[data-toggle="modal"]');
  		if(obj == null || !isReviewed) {
  			return;
  		}
  		obj.click();
  	}
  	
  	void showModalAccepted() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="accepted"] a[data-toggle="modal"]');
  		if(obj == null || !isAccepted) {
  			return;
  		}
  		obj.click();
  	}
  	
  	void showModalFinal() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="final"] a[data-toggle="modal"]');
  		if(obj == null || !isFinal) {
  			return;
  		}
  		obj.click();
  	}
  	
  	void showModalRejected() {
  		var obj = querySelector('#paper-status-' + paperId + ' ph[dime-placeholder="rejected"] a[data-toggle="modal"]');
  		if(obj == null || !isRejected) {
  			return;
  		}
  		obj.click();
  	}
    
	String getSubmittedClass() {
		return getClassFor(isSubmitted);
	}
	
	String getAssignedClass() {
		return getClassFor(isAssigned);
	}
	
	String getReviewedClass() {
		return getClassFor(isReviewed);
	}
	
	String getAcceptedClass() {
		return getClassFor(isAccepted);
	}
	
	String getFinalClass() {
		return getClassFor(isFinal);
	}
	
	String getRejectedClass() {
		if (isRejected)
			return "is-rejected";
		return "";
	}
	
	String getClassFor(bool flag) {
	 	if (flag)
			return "";
		return "item-disabled";
	}
	
	String getSubmittedTooltip() {
		return isSubmitted ? "Submitted" : "Not Yet Submitted";
	}
	
	String getAssignedTooltip() {
		return isAssigned ? "Assigned" : "Not Yet Assigned";
	}
	
	String getReviewedTooltip() {
		return isReviewed ? "Reviewed" : "Not Yet Reviewed";
	}
	
	String getAcceptedTooltip() {
		return isAccepted ? "Accepted" : "Not Yet Accepted";
	}
	
	String getFinalTooltip() {
		return isFinal ? "Final" : "Not Yet Final";
	}
}
