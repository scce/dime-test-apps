import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;

@Component(
	selector: "ReviewResult",
	templateUrl: 'review_result.html',
	styleUrls: const ["review_result.css"],
	directives: [coreDirectives, forms.formDirectives]
)

class ReviewResult {
  
	@Input() dynamic evaluation;
	@Input() dynamic confidence;
	
	String getReviewResultClass() {
		return "";
	}
		
	String getEvaluationClass() {
		return "";
	}
	
	String getConfidenceClass() {
		return "";
	}
	
	String getTooltip() {
		return "${evaluation} - Confidence: ${confidence}";
	}
	
	String getEvaluationBubble1Class() {
		String cls = "empty ion-android-radio-button-off";
		String eval = getValue(evaluation);
		if (eval == "reject")
			cls = "reject-minus ion-android-remove-circle";
		return getBubbleClass(cls);
	}
	
	String getEvaluationBubble2Class() {
		String cls = "empty ion-android-radio-button-off";
		String eval = getValue(evaluation);
		if (eval == "reject")
			cls = "reject-filled ion-android-radio-button-on";
		if (eval == "weakreject")
			cls = "weakreject-minus ion-android-remove-circle";
		return getBubbleClass(cls);
	}
	
	String getEvaluationBubble3Class() {
		String cls = "empty ion-android-radio-button-off";
		String eval = getValue(evaluation);
		if (eval == "reject")
			cls = "reject-filled ion-android-radio-button-on";
		if (eval == "weakreject")
			cls = "weakreject-filled ion-android-radio-button-on";
		if (eval == "indifferent")
			cls = "indifferent ion-android-radio-button-on";
		if (eval == "weakaccept")
			cls = "weakaccept-filled ion-android-radio-button-on";
		if (eval == "accept")
			cls = "accept-filled ion-android-radio-button-on";
		return getBubbleClass(cls);
	}
	
	String getEvaluationBubble4Class() {
		String cls = "empty ion-android-radio-button-off";
		String eval = getValue(evaluation);
		if (eval == "weakaccept")
			cls = "weakaccept-plus ion-android-add-circle";
		if (eval == "accept")
			cls = "accept-filled ion-android-radio-button-on";
		return getBubbleClass(cls);
	}
	
	String getEvaluationBubble5Class() {
		String cls = "empty ion-android-radio-button-off";
		String eval = getValue(evaluation);
		if (eval == "accept")
			cls = "accept-plus ion-android-add-circle";
		return getBubbleClass(cls);
	}
	
	String getBubbleClass(String suffix) {
		return "review-result-evaluation-bubble-${suffix}";
	}
	
	String getConfidenceBarClass() {
		String conf = getValue(confidence);
		return "review-result-confidence-bar-${conf}";
	}
	
	String getValue(dynamic obj) {
		return obj.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "").toLowerCase();
	}
}
