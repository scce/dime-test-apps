import 'package:angular/angular.dart';
import 'dart:js' as js;
import 'dart:async';


@Component(
    selector: 'ExternalPageFrame',
    templateUrl: 'external_page_frame.html',
    styleUrls: const ['external_page_frame.css'],
    directives: [coreDirectives]
)
class ExternalPageFrame implements OnInit {
	
  @Input()
  int maxWidth = null;

  final _back = new StreamController<Map<String,String>>();
  @Output() Stream<dynamic> get back => _back.stream;
  
  final _terms = new StreamController<Map<String,String>>();
  @Output() Stream<dynamic> get terms => _terms.stream;
  
  final _legal = new StreamController<Map<String,String>>();
  @Output() Stream<dynamic> get legal => _legal.stream;
  
  final _privacy = new StreamController<Map<String,String>>();
  @Output() Stream<dynamic> get privacy => _privacy.stream;
  
  final _accessibility = new StreamController<Map<String,String>>();
  @Output('accessibility') Stream<dynamic> get evt_accessibility => _accessibility.stream;

  ExternalPageFrame() {
    
  }

  @override
  ngOnInit() {
    
  }
  
  void toTermsAndConditions(dynamic e) {
  	e.preventDefault();
  	_terms.add({});
  }
  
   void toLegal(dynamic e) {
  	e.preventDefault();
  	_legal.add({});
  }
  
   void toPrivacy(dynamic e) {
  	e.preventDefault();
  	_privacy.add({});
  }
  
  void toAccessibility(dynamic e) {
   e.preventDefault();
    _accessibility.add({});
  }
  
  void triggerBack(dynamic e) {
  	e.preventDefault();
  	_back.add({});
  }
  
  void scrollTo(dynamic e,String id) {
  	e.preventDefault();
  	js.context.callMethod("landing_page_scrollTo",[id]);
  }
  
  Map<String,String> getStyle() {
  /*
  	if(fitToContent) {
  		return {
		    'height':'auto',
		    'padding-bottom': '150px'
		  };
  	}
  	*/
  	return {};
  }
  
   Map<String,String> getContentStyle() {
  	if (maxWidth == null || maxWidth == 0) {
  	    return { 'max-width':'none' };
  	}
  	else {
  	    return { 'max-width': maxWidth.toString() + 'px' };  	    
  	}
  }
  
  
}
