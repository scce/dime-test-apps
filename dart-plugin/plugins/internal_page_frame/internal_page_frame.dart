import 'package:angular/angular.dart';
import 'dart:async';
import 'dart:html';
import 'dart:js' as js;
//Import Services
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
//import 'package:app/models/Selectives.dart';

@Component(
  selector: "InternalPageFrame",
  templateUrl: 'internal_page_frame.html',
  styleUrls: const ['internal_page_frame.css'],
  encapsulation: ViewEncapsulation.None,
  directives: [coreDirectives]
)

class InternalPageFrame implements OnInit, AfterViewChecked {

  @Input()
  String firstname;

  @Input()
  String lastname;

  @Input()
  String username;

  @Input()
  dynamic user;

  @Input()
  dynamic userSince;

  @Input()
  String selectedConference;

  @Input()
  dynamic conferencePhase;
  
  @Input()
  dynamic conference;

  @Input()
  bool canConfigure;
  
  @Input()
  String textConfigure;

  @Input()
  int countConfigure;

  @Input()
  bool canOverview;

  @Input()
  int countOverview;
  
  @Input()
  String textOverview;

  @Input()
  bool canBid;

  @Input()
  int countBid;
  
  @Input()
  String textBid;

  @Input()
  bool canAssign;

  @Input()
  int countAssign;
  
  @Input()
  String textAssign;

  @Input()
  bool canReview;

  @Input()
  int countReview;
  
  @Input()
  String textReview;
  
  @Input()
  bool canDiscuss;

  @Input()
  int countDiscuss;
  
  @Input()
  String textDiscuss;

  @Input()
  bool canDecide;

  @Input()
  int countDecide;
  
  @Input()
  String textDecide;
  
  @Input()
  bool canProduce;

  @Input()
  int countProduce;
  
  @Input()
  String textProduce;

  @Input()
  String conferenceColor;

  @Input()
  int messagesNotifications;

  @Input()
  int newsNotifications;

  @Input()
  int countNotifications;

  @Input()
  List tracks;
  
  final _showMessages = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showMessages => _showMessages.stream;
  
  final _showNotifications = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showNotifications => _showNotifications.stream;

  final _showProfile = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showProfile => _showProfile.stream;

  final _showArchive = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showArchive => _showArchive.stream;
  
  final _showConferences = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showConferences => _showConferences.stream;
  
  final _showConference = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showConference => _showConference.stream;
  
  final _showIntent = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get showIntent => _showIntent.stream;
  
  final _submitToTrack = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get submitToTrack => _submitToTrack.stream;
  
  final _switchConference = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get switchConference => _switchConference.stream;
  
  final _legal = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get legal => _legal.stream;
  
  final _terms = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get terms => _terms.stream;
  
  final _privacy = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get privacy => _privacy.stream;
  
  final _accessibility = new StreamController<Map<String,dynamic>>();
  @Output() Stream<dynamic> get accessibility => _accessibility.stream;

  bool hasBeenLoaded = false;

  InternalPageFrame() {}
  
  @override
  void ngOnInit() {
  	initializeDateFormatting(window.navigator.language,null).then((_)=>Intl.defaultLocale = window.navigator.language);
  }

  bool isConferenceSelected(){
    if(selectedConference==null){
      return false;
    }
    if(selectedConference.isEmpty){
      return false;
    }
    return true;
  }
  
  String getNotificationMessage(int n){
  	if(n>5){
  		return "First 5 of ${n} new notifications";
  	}
  	return "You have ${n} new notifications";
  }

  String getConferenceColor() {
    if(conferenceColor==null){
      return '#f9fafc';
    }
    if(conferenceColor.isEmpty){
      return '#f9fafc';
    }
    return "#${conferenceColor}";
  }
  
  void toTerms(dynamic evt) {
  	evt.preventDefault();
    _terms.add({});
  }
  
  void toLegal(dynamic evt) {
  	evt.preventDefault();
    _legal.add({});
  }
  
  void toPrivacy(dynamic evt) {
  	evt.preventDefault();
    _privacy.add({});
  }
  
  void toAccessibility(dynamic e) {
   e.preventDefault();
    _accessibility.add({});
  }
  
  void clickSwitchConference(dynamic evt) {
  	evt.preventDefault();
    _switchConference.add({'user':user});
  }

  void clickShowMessages(dynamic evt) {
    evt.preventDefault();
    _showMessages.add({'user':user});
  }

  void clickShowNotifications(dynamic evt) {
    evt.preventDefault();
    _showNotifications.add({'user':user});
  }

  void clickShowProfile(dynamic evt) {
    evt.preventDefault();
    _showProfile.add({
      'user':user
    });
  }

  void clickShowArchive(dynamic evt) {
    evt.preventDefault();
    _showArchive.add({'user':user});
  }
  
  void clickSubmitTrack(track,dynamic evt) {
    evt.preventDefault();
    _submitToTrack.add({'manuscriptService':track});
  }
  
  void clickShowConference(dynamic evt) {
    evt.preventDefault();
    _showConference.add({'conference':conference});
  }
  
  void clickShowConferences(dynamic evt) {
    evt.preventDefault();
    _showConferences.add({'user':user});
  }
  
  void clickShowIntent(dynamic evt,String intent) {
    print("clickShowIntent: $intent");
    evt.preventDefault();
    _showIntent.add({
    'conference':conference,
    'intent':intent
    });
  }


  @override
  ngAfterViewChecked() {
	 if(!hasBeenLoaded) {
	 	hasBeenLoaded = true;
	    js.context.callMethod('reloadEntireWindow',[]);
	    js.context.callMethod('reloadWindowSize',[]);
    }
  }
}

