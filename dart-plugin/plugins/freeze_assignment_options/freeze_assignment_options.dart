import 'package:angular/angular.dart';
import 'dart:async';

@Component(
  selector: 'FreezeAssignmentOptions',
  templateUrl: 'freeze_assignment_options.html',
  styleUrls: const ["freeze_assignment_options.css"],
  directives: [coreDirectives]
)

class FreezeAssignmentOptions {
  
  bool custom = false;
  int minAssignees = 1;

  @Output('syncminAssignees')
  Stream<dynamic> get evt_syncminAssignees => syncminAssigneesstream.stream;
  StreamController<dynamic> syncminAssigneesstream = new StreamController();

  final _selectionChanged = new StreamController<dynamic>();
  @Output() Stream<dynamic> get selectionChanged => _selectionChanged.stream;
  
  void notify() {
    _selectionChanged.add({
      'custom': custom,
      'minAssignees': minAssignees
    });
  }
  
  void selectComplete($event) {
    custom = false;
    notify();
  }
	
  void selectCustom($event) {
    custom = true;
    notify();
  }

  void updateMinAssignees($event) {
    minAssignees = int.parse($event.target.value);
    if (custom) notify();
  }
}
