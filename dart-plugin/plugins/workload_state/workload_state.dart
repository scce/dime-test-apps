import 'package:angular/angular.dart';

@Component(
  selector: "WorkloadState",
  templateUrl: 'workload_state.html',
  directives: [coreDirectives]
)

class WorkloadState {
  
  	@Input()
  	int workload;
  	
  	@Input()
  	int warnIndex;
  	
  	@Input()
  	int width;
  	
  	int criticalMultiplier = 150;

	int getPercentage() {
		var max = warnIndex * 2;
		if (workload >= max) {
			return 100;
		}
		return (workload / max * 100).round();
	}
	
	String getColorClass() {
		var criticalIndex = warnIndex * criticalMultiplier / 100;
		if (workload >= criticalIndex)
			return 'progress-bar-danger';
		if (workload >= warnIndex)
			return 'progress-bar-warning';
		return 'progress-bar-success';
	}
	
}
