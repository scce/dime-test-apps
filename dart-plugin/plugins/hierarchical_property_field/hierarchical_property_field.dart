import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';

@Component(
    selector: 'HierarchicalPropertyField',
    templateUrl: 'hierarchical_property_field.html',
    styleUrls: const ["hierarchical_property_field.css"],
    directives: [coreDirectives,forms.formDirectives]
)
class HierarchicalPropertyField implements OnInit {

  @Input() String propertyName;
  @Input() bool inlineParentValue;
  @Input() bool inlineCustomValue;
  @Input() dynamic parent;
  @Input() String parentName;
  @Input() bool isInherited;

  @Output('syncisInherited') Stream<Map<String, dynamic>> get evt_syncisInherited => syncisInheritedstream.stream;
  StreamController<Map<String, dynamic>> syncisInheritedstream = new StreamController();

  bool parentExists;
  int optionId;
  bool wrapWithPanel;
  bool showCustomValueLabel;

  void ngOnInit() {
    optionId = hashCode;
    parentExists = (parent != null || isInherited);
  	if (parentName == null || parentName == '') {
  		parentName = 'parent';
  	}
    wrapWithPanel = (parentExists || !inlineCustomValue);
    showCustomValueLabel = (parentExists || inlineCustomValue);
  }

  void select(event) {
    isInherited = (event.target.value == "inherit");
    syncisInheritedstream.add({ 'isInherited' : isInherited });
  }

  String customValueLabel() {
  	if (parentExists) {
      return isInherited ? 'Custom value' : 'Custom value:';
  	}
  	if (inlineCustomValue) {
  	  return '${propertyName}:';
  	}
  	return propertyName;
  }
}