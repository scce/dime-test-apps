import 'package:angular/angular.dart';

@Component(
	selector: "AssignmentState",
	templateUrl: 'assignment_state.html',
	styleUrls: const ["assignment_state.css"],
	directives: [coreDirectives]
)

class AssignmentState {
  
	@Input() int minAssignees;
	@Input() int numAssignees;
	@Input() bool isFrozen;
	
	String getStateClass() {
		if (isFrozen)
			return "assign-state-frozen";
		return "";
	}
	
	String getIndicatorClass() {
		if (numAssignees >= minAssignees)
			return "state-indicator-complete";
		if (numAssignees > 0)
			return "state-indicator-partial";
		return "state-indicator-unassigned";
	}
	
	String getTooltip() {
		String tip = "";
		if (numAssignees >= minAssignees)
			tip = "completely assigned: ${numAssignees}/${minAssignees}";
		else if (numAssignees > 0)
			tip = "partially assigned: ${numAssignees}/${minAssignees}";
		else
			tip = "not assigned: ${numAssignees}/${minAssignees}";
		if (isFrozen)
			tip += " (frozen)";
		return tip;
	}
}
