import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';
import 'dart:html';

@Component(
    selector: 'KeywordList',
    templateUrl: 'keyword_list.html',
    styleUrls: const ["keyword_list.css"],
    directives: [coreDirectives, forms.formDirectives]
)

class KeywordList {
  
  @Input() List<String> keywords;
  
  @Input() bool deletable;
  
  final _removeKeyword = new StreamController<dynamic>();
  @Output() Stream<dynamic> get removeKeyword => _removeKeyword.stream;
  
  void doRemoveKeyword(dynamic evt, int index) {
    evt.preventDefault();
    print('The selected index is: $index');
    _removeKeyword.add({
      'index':index
    });
  }
  
  String getValueContainerClass() {
    if (deletable) {
      return "value-container-deletable";
    }
    else return null;
  }
}
