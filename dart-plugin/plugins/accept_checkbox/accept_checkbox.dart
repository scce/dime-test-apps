import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart' as forms;
import 'dart:async';


@Component(
    selector: 'AcceptCheckbox',
    templateUrl: 'accept_checkbox.html',
    directives: [coreDirectives,forms.formDirectives]
)
class AcceptCheckbox {

  bool state = false;
  
  @Input('accept')
  bool accept;

  @Output('syncaccept') Stream<Map<String,dynamic>> get evt_syncaccept => syncacceptstream.stream;
  StreamController<Map<String,dynamic>> syncacceptstream = new StreamController();
  
  void changed(bool value) {
  	syncacceptstream.add({ 'accept':value });
  }
 
}
