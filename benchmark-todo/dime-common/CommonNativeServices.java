package info.scce.dime.common;

import de.ls5.dywa.api.FileController;
import de.ls5.dywa.api.ObjectController;
import de.ls5.dywa.entities.object.DBField;
import de.ls5.dywa.entities.object.DBFile;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

<<<<<<< HEAD:info.scce.dime.examples/examples/TODO-app/dime-common/CommonNativeServices.java
public class CommonNativeServices {
=======
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import java.io.File;
import java.io.InputStream;

public class Atomic {
		
	public static void sleepForSomeTime(Long millis) {
	   try {
	      Thread.currentThread().sleep(millis);
	   }
	   catch (InterruptedException e) {
	      throw new RuntimeException(e);
	   }
	}

	public static de.ls5.dywa.generated.util.FileReference uploadProjectResource(final String path) {
		final BeanManager bm = CDI.current().getBeanManager();

		final Bean<FileController> bean = (Bean<FileController>) bm.resolve(bm.getBeans(FileController.class));
		final CreationalContext<FileController> cctx = bm.createCreationalContext(bean);
		final FileController fileController = (FileController) bm.getReference(bean, bean.getBeanClass(), cctx);

		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
		final String[] pathParts = path.split(File.pathSeparator);
		final String fileName = pathParts[pathParts.length - 1];

		final DBFile file = fileController.createFile(fileName, inputStream);

		return new de.ls5.dywa.generated.util.FileReference(file);
	}
	
	public static String convertToText(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString();
	}
}
