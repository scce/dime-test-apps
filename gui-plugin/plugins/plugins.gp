import "dime-models/app.data" as app

plugin test_plugin {
	styles ["plugins/test_plugin/style.css"]
	scripts ["plugins/test_plugin/script.js"]
	template "plugins/test_plugin/template.html"
	function testFunction {
		primitiveParameter "someText": text
		primitiveParameter "someInt": integer
	}
}

plugin wordCounter {
	scripts ["plugins/word_counter/word_counter.js"]
	function enableWordCounter {
		primitiveParameter "dimeAttribute" : text
		primitiveParameter "maxWordCount" : integer
	}
}

plugin input_test {
	scripts ["plugins/input_test/script.js"]
	template "plugins/input_test/template.html"
	function inputTestFunction {
		primitiveParameter "someText": text
		primitiveParameter "someTextStatic": text
		primitiveParameter "someInt": integer
		primitiveParameter "someIntStatic": integer
		complexParameter "someComplexParam": app.ConcreteUser
	}
}