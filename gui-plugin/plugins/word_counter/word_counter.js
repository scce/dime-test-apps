/**
 * Created by zweihoff on 08.03.17.
 */
function enableWordCounter(dimeAttribute,maxWordCount)
{
    $('textarea[dime="'+dimeAttribute+'"]').each(function () {
        //$(this).on('keyup',function () {
        //    updateCounter(this,maxWordCount);
        //});
        if($('#word-counter').length >0){
            updateCounter(this,maxWordCount);
        }
        else {
	        createCounter(this,maxWordCount);        
        }
        
    });
}
function createCounter(element,maxWordCount){
	var wordCount = $(element).val().length;
    $(element).after('<div id="word-counter" class="row"><div class="col-sm-12 text-right"><span class="word-counter">'+wordCount+'/'+maxWordCount+'</span></div></div>');

}
function updateCounter(element,maxWordCount)
{
    var wordCount = $(element).val().length;
    var counterElement = $(element).next().find('.word-counter')[0];
    $(counterElement).empty();
    $(counterElement).append(wordCount+'/'+maxWordCount);
}
