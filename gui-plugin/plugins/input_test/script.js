var someComplexParamGlobal = null;

function inputTestFunction(someText, someTextStatic, someInt, someIntStatic, someComplexParam) {
  $('#someText').text(someText);
  $('#someTextStatic').text(someTextStatic);
  $('#someInt').text(someInt);
  $('#someIntStatic').text(someIntStatic);
  $('#someComplexParam').text(someComplexParam);
  someComplexParamGlobal = someComplexParam;
}
