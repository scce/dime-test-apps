#!/usr/bin/env python3.6
import http.client
import json
import os


def make_request(text):
    connection = http.client.HTTPSConnection(host='gitlab.com')
    connection.request(
        method="POST",
        url="/api/v4/ci/lint",
        headers={'Content-type': 'application/json'},
        body=json.dumps({'content': text}),
    )
    response = connection.getresponse()
    return connection, response


def check_status_code(connection, response):
    if response.status != 200:
        failed(connection, 'Linting failed (status code {})'.format(response.status))


def process_response(connection, response):
    body = read_response_body(response=response)
    if body['status'] != 'valid':
        invalid(
            conn=connection,
            message='Linting of .gitlab-ci.yml returned errors',
            errors=body['errors'],
        )


def read_response_body(response):
    return json.loads(response.read().decode())


def failed(conn, message):
    print("✖ " + message)
    conn.close()
    exit(1)


def invalid(conn, message, errors):
    print("✖ " + message + "\n")
    print(*map(lambda error: '• ' + error, errors), sep="\n")
    conn.close()
    exit(1)


def valid(conn):
    print("✓ Linting of .gitlab-ci.yml returned no errors")
    conn.close()
    exit(0)


def main():
    script_directory = os.path.dirname(os.path.realpath(__file__))
    gitlab_ci_file_path = os.path.join(script_directory, '..', '.gitlab-ci.yml')
    with open(gitlab_ci_file_path) as json_file:
        text = json_file.read()
        connection, response = make_request(text)
        check_status_code(connection, response)
        process_response(connection, response)
        valid(connection)


if __name__ == "__main__":
    main()
