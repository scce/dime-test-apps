let pkgs = import (builtins.fetchTarball {
        name = "nixos-20.03-252bfe0107587d40092057f338e9ffcf7bbd90cb";
        url = "https://github.com/nixos/nixpkgs/archive/252bfe0107587d40092057f338e9ffcf7bbd90cb.tar.gz";
        sha256 = "1ljw98lcc04mlz6pprlgd1plinwl5q8fraakk6bx8igkiqlxaadn";
    }) {};

in rec {
    guile-json =
        let pname = "guile-json";
            version = "90b60b9739b80b64806c78cf6868ebd5dbd3fda3";
        in pkgs.stdenv.mkDerivation {
            inherit pname version;

            src = pkgs.fetchurl {
                url = "https://github.com/aconchillo/${pname}/archive/${version}.tar.gz";
                sha256 = "0jcqpwixl3wkczc9x1xbjx7kqdbwr3xv7js6bn3li3481b7pqcfp";
            };

            nativeBuildInputs = with pkgs; [ autoconf automake pkgconfig ];

            buildInputs = with pkgs; [ guile ];

            preConfigure = ''
                autoreconf -vif
            '';
        };

    generate = pkgs.writeShellScriptBin "generate-gitlab-ci" ''
        export GUILE_LOAD_PATH="${guile-json}/share/guile/site/2.2"
        export GUILE_LOAD_COMPILED_PATH="${guile-json}/share/guile/site/2.2"
        ${pkgs.guile}/bin/guile ci/gitlab-ci.scm
    '';

    lint = pkgs.writeShellScriptBin "lint-gitlab-ci" ''
        export PYTHONIOENCODING=UTF-8
        ${pkgs.python36}/bin/python ci/lint.py
    '';
}
