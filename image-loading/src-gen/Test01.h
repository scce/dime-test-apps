// C header for process "Test01"

#ifndef SLG_Test01_H
#define SLG_Test01_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "slg.h"



// result type for slg.
typedef struct slg_Test01_return_s {
	int branch;
} slg_Test01_return_st;

// free the 'static' data structures like the adjacency information.
void slg_Test01_free();

// init 'static' data structures like the adjacency information.
void slg_Test01_init();

// main process execution function.
slg_Test01_return_st* slg_Test01_execute();

#endif // SLG_Test01_H
