// C implementation for process "Test01"

#include "Test01.h"



// attributes shaping the context variables.
typedef struct slg_Test01_context_s {
	// context variables.
	 type;
} slg_Test01_context_st;

// function pointer type definition sib execution. 
typedef int (*slg_Test01_sibs_fpt) (slg_Test01_context_st*);
// array of sibs.
slg_Test01_sibs_fpt* slg_Test01_sibs = NULL;
// mapping of branch count per sib.
int* slg_Test01_branch_counts = NULL;
// adjacency information mapping from sib to their branches and from there to the target sib. 
int** slg_Test01_adjacencies = NULL;

// function pointers for returning results of a process
typedef slg_Test01_return_st* (*slg_Test01_return_fpt) (slg_Test01_context_st*);
slg_Test01_return_fpt* slg_Test01_returns = NULL;

// the sib function implementations.
// =================================


// implementation for SIB 'PutToContext' putting values to context.
int slg_Test01_PutToContext(slg_Test01_context_st* ctx) {
	// put 'input2'.
	slg_release((void*)slg_text_new("image1"), &slg_text_release_deps);
	return 0; //success
}



// end of sib function implementations.
// ====================================


void slg_Test01_init() {
	// create an array of function pointers for the sib containers.
	if (slg_Test01_sibs == NULL) {
		slg_Test01_sibs = calloc(3, sizeof(slg_Test01_sibs_fpt));
		slg_Test01_sibs[0] = &slg_Test01_Test01_Upload;
		slg_Test01_sibs[1] = &slg_Test01_PutToContext;
		slg_Test01_sibs[2] = &slg_Test01_Test01_1;
	}
	
	// create an array that holds the information which sib has how many outgoing branches.
	if (slg_Test01_branch_counts == NULL) {
		slg_Test01_branch_counts = calloc(3, sizeof(int));
		slg_Test01_branch_counts[0] = 1;
		slg_Test01_branch_counts[1] = 1;
		slg_Test01_branch_counts[2] = 0;
	}
	
	// Create an 2-dimensional array with the adjacency information. The first dimension represents the sib.
	// The second dimension represents the branch. The value represents the index of the target sib in the array
	// of function pointers in 'slg_Test01_sibs'.
	if (slg_Test01_adjacencies == NULL) {
		slg_Test01_adjacencies = calloc(3, sizeof(int*));
			slg_Test01_adjacencies[0] = calloc(1, sizeof(int));
			
			slg_Test01_adjacencies[0][0] = 1;
			
			slg_Test01_adjacencies[1] = calloc(1, sizeof(int));
			
			slg_Test01_adjacencies[1][0] = 2;
			
			slg_Test01_adjacencies[2] = NULL;
			
	}
	
}

// frees 'static' resources of this process.
void slg_Test01_free() {

	if (slg_Test01_sibs != NULL) {
	    free(slg_Test01_sibs);
	    slg_Test01_sibs = NULL;
	}

	if (slg_Test01_branch_counts != NULL) {
	    free(slg_Test01_branch_counts);
	    slg_Test01_branch_counts = NULL;
	}

    if (slg_Test01_adjacencies != NULL) {
		free(slg_Test01_adjacencies[0]);
		free(slg_Test01_adjacencies[1]);
		free(slg_Test01_adjacencies);
		slg_Test01_adjacencies = NULL;
	}
	
	if (slg_Test01_returns != NULL) {
		free(slg_Test01_returns);
		slg_Test01_returns = NULL;
	}

}

// main process execution function.
slg_Test01_return_st* slg_Test01_execute() {
    if(!slg_Test01_sibs) {
        slg_Test01_init();
    }

    // set first sib as the start sib.
    int currentSIB = 0;

    // initialize the process context.
    slg_Test01_context_st* ctx = calloc(1, sizeof(slg_Test01_context_st));
	ctx->type = ;
    
	
	while (true) {
		// Execute sib.
	    int branch = slg_Test01_sibs[currentSIB](ctx);
	    // Check whether an end sib has been reached.
		if (branch < 0) {
			// End sibs return the negative value minus one of the 
			// branch index to return to one hierarchy level above.
			int returnBranch = (branch + 1) * -1;
			// Build the return value
			slg_Test01_return_st* returnValue = slg_Test01_returns[returnBranch](ctx);
			return returnValue;
	    }
		// Check whether there is a successor sib ...
		else if (branch < slg_Test01_branch_counts[currentSIB]) {
			// Retrieve index of next sib.
			currentSIB = slg_Test01_adjacencies[currentSIB][branch];
		}
		// ... or else return an error code.
	    else {
			slg_Test01_return_st* returnValue = calloc(1, sizeof(slg_Test01_return_st));
			returnValue->branch = -1;
			free(ctx);
			return returnValue;
	    }
	}
}
